create table customer
(
h string,
name string,
cust_i int,
open_dt string,
consul_dt string,
vac_id string,
dr_name string,
state string,
country string,
dob string,
flag string
) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' with SERDEPROPERTIES ( "separatorChar" = "|")
stored as textfile;

load data local inpath '/home/bigdata/data/cust.csv' into table customer;

//Country
CREATE TABLE t_country 
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' 
STORED AS TEXTFILE AS
WITH t AS(
SELECT DISTINCT country
FROM customer)
SELECT country
FROM t;

//Join query
select testtable.name from testtable left join t_country on testtable.country=t_country.country
